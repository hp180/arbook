﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{

    private Image backgroundImage;
    private Image joystickImage;
    private Vector3 inputVector;
    private bool isCurrentlyActive = false;

    // Start is called before the first frame update
    void Start()
    {
        backgroundImage = transform.GetChild(0).GetComponent<Image>();
        joystickImage = transform.GetChild(0).GetChild(0).GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 position;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImage.rectTransform,
                                                                    eventData.position,
                                                                    eventData.pressEventCamera,
                                                                    out position))
        {
            position.x /= backgroundImage.rectTransform.sizeDelta.x;
            position.y /= backgroundImage.rectTransform.sizeDelta.y;

            inputVector = new Vector3(position.x * 2 + 1, 0, position.y * 2 - 1);

            // normalize input vector
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            // move joystick image
            joystickImage.rectTransform.anchoredPosition = new Vector3( inputVector.x * (backgroundImage.rectTransform.sizeDelta.x / 3), 
                                                                        inputVector.z * (backgroundImage.rectTransform.sizeDelta.y / 3));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
        isCurrentlyActive = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // reset input vector
        inputVector = Vector3.zero;

        // set joystick image back to center
        joystickImage.rectTransform.anchoredPosition = Vector3.zero;

        isCurrentlyActive = false;
    }

    public bool getIsCurrentlyActive()
    {
        return isCurrentlyActive;
    }

    public float Horizontal()
    {
        if (inputVector.x != 0)
        {
            return inputVector.x;
        }
        else
        {
            return Input.GetAxis("Horizontal");
        }
    }

    public float Vertical()
    {
        if (inputVector.x != 0)
        {
            return inputVector.z;
        }
        else
        {
            return Input.GetAxis("Horizontal");
        }
    }

}
