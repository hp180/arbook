﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedGif : MonoBehaviour
{
    public Sprite[] animatedImages;
    public Image animatedImageObject;
    public int fps = 20;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int index = (int) (Time.time * fps) % animatedImages.Length;

        animatedImageObject.sprite = animatedImages[index];
    }
}
