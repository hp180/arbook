﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Page4Main : MonoBehaviour, ITrackableEventHandler
{
    // class elements:


    // Vufuria elements:
    private TrackableBehaviour mTrackableBehavior;


    // Start is called before the first frame update
    void Start()
    {

        SetupVuforiaComponents();
    }

    void SetupVuforiaComponents()
    {
        // setup trackable behavior and assign event handler
        mTrackableBehavior = GetComponent<TrackableBehaviour>();
        if (mTrackableBehavior)
        {
            mTrackableBehavior.RegisterTrackableEventHandler(this);
        }
    }


    // Update is called once per frame
    void Update()
    {

    }



    // ###########################################################################################################
    // ##################################### ITrackableEventHandler ##############################################
    // ###########################################################################################################

    // used to activate/deactivate gameobjects and the application of gravity in order to avoid undesired movement
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    private void OnTrackingFound()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();


        for (int i = 0; i < transform.childCount; i++)
        {
            Debug.Log("Activating the children");
            this.transform.GetChild(i).gameObject.SetActive(true);
        }

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = true;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " found");
    }

    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();


        for (int i = 0; i < transform.childCount; i++)
        {
            Debug.Log("Deactivating children");
            this.transform.GetChild(i).gameObject.SetActive(false);
        }

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }


        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = false;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " lost");

    }
}
