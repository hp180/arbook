﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Page3Main : MonoBehaviour, IVirtualButtonEventHandler, ITrackableEventHandler
{
    // class elements:
    public GameObject dragon;
    public GameObject frog;
    public GameObject spider;
    public float rotationSpeed = 20.0f;
    public GameObject gestureIndicatorPanel;

    private Animator frogAnimationComponent;
    private Animator dragonAnimationComponent;
    private Animator spiderAnimationComponent;

    // Vuforia elements:
    private readonly float userInteractionDisableTime = 1.5f;
    private GameObject currentlyActiveAnimal;

    private TrackableBehaviour mTrackableBehavior;
    private GameObject virtualButton_show_dragon;
    private GameObject virtualButton_show_frog;
    private GameObject virtualButton_show_spider;
    private GameObject virtualButton_swap;
    private GameObject virtualButton_play_sound;
    private GameObject virtualButton_play_animation;

    // Start is called before the first frame update
    void Start()
    {
        SetupVuforiaComponents();

        HideDetailVirtualButtons();

        frogAnimationComponent = frog.GetComponent<Animator>();
        dragonAnimationComponent = dragon.GetComponent<Animator>();
        spiderAnimationComponent = spider.GetComponent<Animator>();
    }

    void SetupVuforiaComponents()
    {
        // setup virtual buttons and assign event handlers
        Debug.Log("Adding virtual button event handlers.");

        virtualButton_show_dragon = GameObject.Find("VirtualButton_P3_Show_Dragon");
        if (virtualButton_show_dragon)
        {
            virtualButton_show_dragon.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_show_frog = GameObject.Find("VirtualButton_P3_Show_Frog");
        if (virtualButton_show_frog)
        {
            virtualButton_show_frog.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_show_spider = GameObject.Find("VirtualButton_P3_Show_Spider");
        if (virtualButton_show_spider)
        {
            virtualButton_show_spider.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_swap = GameObject.Find("VirtualButton_P3_Swap");
        if (virtualButton_swap)
        {
            virtualButton_swap.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_play_sound = GameObject.Find("VirtualButton_P3_Play_Sound");
        if (virtualButton_play_sound)
        {
            virtualButton_play_sound.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_play_animation = GameObject.Find("VirtualButton_P3_Play_Animation");
        if (virtualButton_play_animation)
        {
            virtualButton_play_animation.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }


        // setup trackable behavior and assign event handler
        mTrackableBehavior = GetComponent<TrackableBehaviour>();
        if (mTrackableBehavior)
        {
            mTrackableBehavior.RegisterTrackableEventHandler(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RotateOnTouchDrag();
    }

    private void RotateOnTouchDrag()
    {
        if (Input.touchCount == 1)
        {
            Touch touchZero = Input.GetTouch(0);

            if (currentlyActiveAnimal)
            {
                // deactivate indicator on first interaction
                gestureIndicatorPanel.SetActive(false);

                //Rotate the model based on offset
                Vector3 localAngle = currentlyActiveAnimal.transform.localEulerAngles;
                localAngle.y -= rotationSpeed * touchZero.deltaPosition.x * Time.deltaTime;
                currentlyActiveAnimal.transform.localEulerAngles = localAngle;
            }
        }
    }


    // ###########################################################################################################
    // ################################### IVirtualButtonEventHandler ############################################
    // ###########################################################################################################

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Debug.Log("pressed");

        string vbName = vb.VirtualButtonName;

        if (vbName == "P3ShowDragon")
        {
            Debug.Log("Virtual Button P3ShowDragon pressed.");
            dragon.SetActive(true);
            frog.SetActive(false);
            spider.SetActive(false);
            HideAnimalSelectionVirtualButtons();

            currentlyActiveAnimal = dragon;

            StartCoroutine(ShowGestureIndicatorDelayed());
        }
        else if (vbName == "P3ShowFrog")
        {
            Debug.Log("Virtual Button P3ShowFrog pressed.");
            dragon.SetActive(false);
            frog.SetActive(true);
            spider.SetActive(false);

            HideAnimalSelectionVirtualButtons();

            currentlyActiveAnimal = frog;

            StartCoroutine(ShowGestureIndicatorDelayed());
        }
        else if (vbName == "P3ShowSpider")
        {
            Debug.Log("Virtual Button P3ShowSpider pressed.");
            dragon.SetActive(false);
            frog.SetActive(false);
            spider.SetActive(true);

            HideAnimalSelectionVirtualButtons();

            currentlyActiveAnimal = spider;

            StartCoroutine(ShowGestureIndicatorDelayed());
        }
        else if (vbName == "P3Swap")
        {
            Debug.Log("Virtual Button P3Swap pressed.");
            HideDetailVirtualButtons();
        }
        else if (vbName == "P3PlaySound")
        {
            Debug.Log("Virtual Button P3PlaySound pressed.");
            if (currentlyActiveAnimal)
            {
                AudioSource audioSource = currentlyActiveAnimal.GetComponent<AudioSource>();
                if (audioSource)
                {
                    if (audioSource.isPlaying == false)
                    {
                        audioSource.Play();
                    }
                }
            }
        }
        else if (vbName == "P3PlayAnimation")
        {
            Debug.Log("Virtual Button P3PlayAnimation pressed.");
            if (currentlyActiveAnimal)
            {
                if (currentlyActiveAnimal == frog)
                {
                    if (frogAnimationComponent)
                    {
                        frogAnimationComponent.Play("Tongue");
                    }
                }
                else if (currentlyActiveAnimal == dragon)
                {
                    if (dragonAnimationComponent)
                    {
                        dragonAnimationComponent.Play("Fly");
                    }
                }
                else if (currentlyActiveAnimal == spider)
                {
                    if (spiderAnimationComponent)
                    {
                        Debug.Log("playing spider animation");
                        spiderAnimationComponent.Play("Attack");
                    }
                }
            }
        }

    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Debug.Log("released");
    }

    private void HideAnimalSelectionVirtualButtons()
    {
        // deactivate
        virtualButton_show_dragon.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_show_frog.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_show_spider.GetComponent<VirtualButtonBehaviour>().enabled = false;

        // hide
        virtualButton_show_dragon.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_show_frog.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_show_spider.transform.GetChild(0).gameObject.SetActive(false);

        ShowDetailVirtualButtons();
    }

    private void ShowAnimalSelectionVirtualButtons()
    {
        // show
        virtualButton_show_dragon.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_show_frog.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_show_spider.transform.GetChild(0).gameObject.SetActive(true);

        // activate
        StartCoroutine(WaitAndEnableSelectionVirtualButtons());
    }

    // waits before enabling buttons for some time to avoid misclicks (especially for buttons in the same location)
    IEnumerator WaitAndEnableSelectionVirtualButtons()
    {
        yield return new WaitForSeconds(userInteractionDisableTime);

        virtualButton_show_dragon.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_show_frog.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_show_spider.GetComponent<VirtualButtonBehaviour>().enabled = true;
    }


    private void HideDetailVirtualButtons()
    {
        virtualButton_swap.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_play_sound.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_play_animation.GetComponent<VirtualButtonBehaviour>().enabled = false;

        virtualButton_swap.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_play_sound.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_play_animation.transform.GetChild(0).gameObject.SetActive(false);

        ShowAnimalSelectionVirtualButtons();
    }

    private void ShowDetailVirtualButtons()
    {
        // show
        virtualButton_swap.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_play_sound.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_play_animation.transform.GetChild(0).gameObject.SetActive(true);

        // activate
        StartCoroutine(WaitAndEnableDetailVirtualButtons());
    }

    // waits before enabling buttons for some time to avoid misclicks (especially for buttons in the same location)
    IEnumerator WaitAndEnableDetailVirtualButtons()
    {
        yield return new WaitForSeconds(userInteractionDisableTime);

        virtualButton_swap.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_play_sound.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_play_animation.GetComponent<VirtualButtonBehaviour>().enabled = true;
    }

    IEnumerator ShowGestureIndicatorDelayed()
    {
        yield return new WaitForSeconds(2);

        gestureIndicatorPanel.SetActive(true);
    }

    // ###########################################################################################################
    // ##################################### ITrackableEventHandler ##############################################
    // ###########################################################################################################

    // used to activate/deactivate gameobjects and the application of gravity in order to avoid undesired movement
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    private void OnTrackingFound()
    {

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();

        // reapply virtual button event handlers
        VirtualButtonBehaviour[] virtualButtonBehaviours = GetComponentsInParent<VirtualButtonBehaviour>();

        for (int i = 0; i < virtualButtonBehaviours.Length; i++)
        {
            virtualButtonBehaviours[i].RegisterEventHandler(this);
        }

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = true;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " found");
    }

    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }


        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = false;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " lost");

    }
}
