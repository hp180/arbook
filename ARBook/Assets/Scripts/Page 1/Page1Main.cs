﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Page1Main : MonoBehaviour, IVirtualButtonEventHandler, ITrackableEventHandler
{
    // class elements:
    public GameObject cow;
    public GameObject duck;
    public GameObject pig;
    public float rotationSpeed = 20.0f;
    public GameObject gestureIndicatorPanel;

    // Vuforia elements:
    private readonly float userInteractionDisableTime = 1.5f;
    private GameObject currentlyActiveAnimal;

    private TrackableBehaviour mTrackableBehavior;
    private GameObject virtualButton_show_cow;
    private GameObject virtualButton_show_duck;
    private GameObject virtualButton_show_pig;
    private GameObject virtualButton_swap;
    private GameObject virtualButton_play_sound;

    // Start is called before the first frame update
    void Start()
    {
        SetupVuforiaComponents();

        HideDetailVirtualButtons();
    }

    void SetupVuforiaComponents()
    {
        // setup virtual buttons and assign event handlers
        Debug.Log("Adding virtual button event handlers.");

        virtualButton_show_cow = GameObject.Find("VirtualButton_P1_Show_Cow");
        if (virtualButton_show_cow)
        {
            virtualButton_show_cow.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_show_duck = GameObject.Find("VirtualButton_P1_Show_Duck");
        if (virtualButton_show_duck)
        {
            virtualButton_show_duck.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_show_pig = GameObject.Find("VirtualButton_P1_Show_Pig");
        if (virtualButton_show_pig)
        {
            virtualButton_show_pig.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_swap = GameObject.Find("VirtualButton_P1_Swap");
        if (virtualButton_swap)
        {
            virtualButton_swap.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }

        virtualButton_play_sound = GameObject.Find("VirtualButton_P1_Play_Sound");
        if (virtualButton_play_sound)
        {
            virtualButton_play_sound.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        }


        // setup trackable behavior and assign event handler
        mTrackableBehavior = GetComponent<TrackableBehaviour>();
        if (mTrackableBehavior)
        {
            mTrackableBehavior.RegisterTrackableEventHandler(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RotateOnTouchDrag();
    }

    private void RotateOnTouchDrag()
    {
        if (Input.touchCount == 1)
        {
            Touch touchZero = Input.GetTouch(0);

            if (currentlyActiveAnimal)
            {
                // deactivate indicator on first interaction
                gestureIndicatorPanel.SetActive(false);

                //Rotate the model based on offset
                Vector3 localAngle = currentlyActiveAnimal.transform.localEulerAngles;
                localAngle.y -= rotationSpeed * touchZero.deltaPosition.x * Time.deltaTime;
                currentlyActiveAnimal.transform.localEulerAngles = localAngle;
            }
        }
    }


    // ###########################################################################################################
    // ################################### IVirtualButtonEventHandler ############################################
    // ###########################################################################################################

    public void OnButtonPressed(VirtualButtonBehaviour vb) {
        Debug.Log("pressed");

        string vbName = vb.VirtualButtonName;

        if (vbName == "P1ShowCow") {
            Debug.Log("Virtual Button P1ShowCow pressed.");
            cow.SetActive(true);
            duck.SetActive(false);
            pig.SetActive(false);
            HideAnimalSelectionVirtualButtons();

            currentlyActiveAnimal = cow;

            StartCoroutine(ShowGestureIndicatorDelayed());
        }
        else if (vbName == "P1ShowDuck")
        {
            Debug.Log("Virtual Button P1ShowDuck pressed.");
            cow.SetActive(false);
            duck.SetActive(true);
            pig.SetActive(false);

            HideAnimalSelectionVirtualButtons();

            currentlyActiveAnimal = duck;

            StartCoroutine(ShowGestureIndicatorDelayed());
        }
        else if (vbName == "P1ShowPig")
        {
            Debug.Log("Virtual Button P1ShowPig pressed.");
            cow.SetActive(false);
            duck.SetActive(false);
            pig.SetActive(true);

            HideAnimalSelectionVirtualButtons();

            currentlyActiveAnimal = pig;

            StartCoroutine(ShowGestureIndicatorDelayed());
        }
        else if (vbName == "P1Swap")
        {
            Debug.Log("Virtual Button P1Swap pressed.");
            HideDetailVirtualButtons();
        }
        else if (vbName == "P1PlaySound")
        {
            Debug.Log("Virtual Button P1PlaySound pressed.");
            if (currentlyActiveAnimal)
            {
                AudioSource audioSource = currentlyActiveAnimal.GetComponent<AudioSource>();
                if (audioSource)
                {
                    if (audioSource.isPlaying == false)
                    {
                        audioSource.Play();
                    }
                }
            }
        }

    }

    public void OnButtonReleased(VirtualButtonBehaviour vb) {
        Debug.Log("released");
    }

    private void HideAnimalSelectionVirtualButtons()
    {
        // deactivate
        virtualButton_show_cow.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_show_duck.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_show_pig.GetComponent<VirtualButtonBehaviour>().enabled = false;

        // hide
        virtualButton_show_cow.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_show_duck.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_show_pig.transform.GetChild(0).gameObject.SetActive(false);

        ShowDetailVirtualButtons();
    }

    private void ShowAnimalSelectionVirtualButtons()
    {
        // show
        virtualButton_show_cow.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_show_duck.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_show_pig.transform.GetChild(0).gameObject.SetActive(true);

        // activate
        StartCoroutine(WaitAndEnableSelectionVirtualButtons());
    }

    // waits before enabling buttons for some time to avoid misclicks (especially for buttons in the same location)
    IEnumerator WaitAndEnableSelectionVirtualButtons()
    {
        yield return new WaitForSeconds(userInteractionDisableTime);

        virtualButton_show_cow.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_show_duck.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_show_pig.GetComponent<VirtualButtonBehaviour>().enabled = true;
    }


    private void HideDetailVirtualButtons()
    {
        virtualButton_swap.GetComponent<VirtualButtonBehaviour>().enabled = false;
        virtualButton_play_sound.GetComponent<VirtualButtonBehaviour>().enabled = false;

        virtualButton_swap.transform.GetChild(0).gameObject.SetActive(false);
        virtualButton_play_sound.transform.GetChild(0).gameObject.SetActive(false);

        ShowAnimalSelectionVirtualButtons();
    }

    private void ShowDetailVirtualButtons()
    {
        // show
        virtualButton_swap.transform.GetChild(0).gameObject.SetActive(true);
        virtualButton_play_sound.transform.GetChild(0).gameObject.SetActive(true);

        // activate
        StartCoroutine(WaitAndEnableDetailVirtualButtons());
    }

    // waits before enabling buttons for some time to avoid misclicks (especially for buttons in the same location)
    IEnumerator WaitAndEnableDetailVirtualButtons()
    {
        yield return new WaitForSeconds(userInteractionDisableTime);

        virtualButton_swap.GetComponent<VirtualButtonBehaviour>().enabled = true;
        virtualButton_play_sound.GetComponent<VirtualButtonBehaviour>().enabled = true;
    }

    IEnumerator ShowGestureIndicatorDelayed()
    {
        yield return new WaitForSeconds(2);

        gestureIndicatorPanel.SetActive(true);
    }

    // ###########################################################################################################
    // ##################################### ITrackableEventHandler ##############################################
    // ###########################################################################################################

    // used to activate/deactivate gameobjects and the application of gravity in order to avoid undesired movement
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    private void OnTrackingFound()
    {

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();

        // reapply virtual button event handlers
        VirtualButtonBehaviour[] virtualButtonBehaviours = GetComponentsInParent<VirtualButtonBehaviour>();

        for (int i = 0; i < virtualButtonBehaviours.Length; i++)
        {
            virtualButtonBehaviours[i].RegisterEventHandler(this);
        }

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = true;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " found");
    }

    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();
        
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }


        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = false;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " lost");

    }
}
