﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wolf : MonoBehaviour
{
    public VirtualJoystick joystick;
    public Button wolfSitButton;

    public float speed = 0.33f;
    public float rotationSpeed = 80;

    private float rotation = 0f;
    private Vector3 moveDirection;
    
    private Rigidbody body;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        wolfSitButton.onClick.AddListener(delegate
        {
            WolfSitButtonClicked();
        });
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void WolfSitButtonClicked()
    {
        //Debug.Log("Sitting triggered");
        if (anim.GetBool("running") == true || anim.GetBool("walking") == true)
        {
            //Debug.Log("running true");
            anim.SetBool("running", false);
            anim.SetInteger("condition", 0);
        }

        if (anim.GetBool("running") == false && anim.GetBool("walking") == false)
        {
            //Debug.Log("running false");
            Sitting();
        }
    }


    private void OnTriggerEnter(Collider col)
    {
        //Debug.Log("##########################");
        if (col.gameObject.name.Contains("bone"))
        {
            transform.parent.GetComponent<Page5Main>().CollisionDetected(col);
        }
    }

    void FixedUpdate()
    {
        if (joystick.getIsCurrentlyActive() == true)
        {
            if (anim.GetBool("sitting") == true)
            {
                return;
            }

            if (anim.GetBool("sitting") == false)
            {
                HandleMovement();
            }
        }

        if (joystick.getIsCurrentlyActive() == false)
        {
            if (anim.GetBool("sitting") == true)
            {
                return;
            }

            if (anim.GetBool("sitting") == false)
            {
                // trigger idle animation
                //Debug.Log("stop running");
                anim.SetBool("walking", false);
                anim.SetBool("running", false);
                anim.SetInteger("condition", 0);

                moveDirection = Vector3.zero;
            }
        }

        HandleRotation();
    }


    void HandleMovement()
    {
        // read joystick inputs
        float h = joystick.Horizontal();
        float v = joystick.Vertical();

        // trigger run animation

        // lower boundary defines controller movement deadzone
        if (v > 0.07 && v < 0.5)
        {
            //Debug.Log("start walking");
            anim.SetBool("walking", true);
            anim.SetInteger("condition", 3);
        }
        else if (v >= 0.5)
        {
            //Debug.Log("start running");
            anim.SetBool("running", true);
            anim.SetInteger("condition", 1);
        }
        else
        {
            return;
        }

        // use world-relative directions
        moveDirection = v * Vector3.forward;

        // make sure y axis is set to zero
        Vector3 moveVector = new Vector3(moveDirection.x, 0, moveDirection.z);

        // apply speed
        moveVector *= Time.deltaTime * speed;

        // transform direction space
        moveVector = transform.TransformDirection(moveVector);

        // move object
        body.MovePosition(transform.position + moveVector);
    }

    void HandleRotation()
    {
        // read joystick inputs
        float h = joystick.Horizontal();

        rotation += h * rotationSpeed * Time.deltaTime;

        // apply rotation locally
        transform.localEulerAngles = new Vector3(transform.rotation.x, rotation, transform.rotation.z);
    }

    void Sitting()
    {
        StartCoroutine(SittingRoutine());
    }

    IEnumerator SittingRoutine()
    {
        Debug.Log("sitting coroutine");

        // trigger sitting animation
        anim.SetBool("sitting", true);
        anim.SetInteger("condition", 2);

        yield return new WaitForSeconds(1);
        //Debug.Log("yield end");

        anim.SetInteger("condition", 0);
        anim.SetBool("sitting", false);
    }
}
