﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Page5Main : MonoBehaviour, IVirtualButtonEventHandler, ITrackableEventHandler
{
    // class elements:
    public GameObject animal;
    public GameObject bonePrefab;
    public GameObject countdownPanel;
    public Text countdownText;
    public GameObject scorePanel;
    public Text scoreText;
    public Button startGameButton;

    private bool countdownTimerIsActive = false;
    private int score = 0;
    private float countdownTime = 40.0f;    // seconds

    // Vufuria elements:
    private TrackableBehaviour mTrackableBehavior;


    // Start is called before the first frame update
    void Start()
    {
        startGameButton.onClick.AddListener(delegate
        {
            StartGameButtonClicked();
        });

        SetupVuforiaComponents();
    }

    void SetupVuforiaComponents()
    {
        // setup trackable behavior and assign event handler
        mTrackableBehavior = GetComponent<TrackableBehaviour>();
        if (mTrackableBehavior)
        {
            mTrackableBehavior.RegisterTrackableEventHandler(this);
        }


    }

    private void StartGameButtonClicked()
    {
        startGameButton.gameObject.SetActive(false);
        countdownPanel.SetActive(true);
        scorePanel.SetActive(true);

        StartGame();
    }

    private void StartGame()
    {
        score = 0;
        countdownTime = 40.0f;

        // start spawning bones
        SpawnBone();

        // start timer
        countdownTimerIsActive = true;
    }


    // Update is called once per frame
    void Update()
    {

        if (countdownTimerIsActive == true)
        {
            CountdownTimer();
            scoreText.text = score.ToString();
        }
    }

    void CountdownTimer()
    {
        countdownTime -= Time.deltaTime;

        if (countdownTime <= 0)
        {
            countdownTime = 0;
            CountdownHasEnded();
        }

        countdownText.text = Mathf.Round(countdownTime).ToString();
    }

    private void CountdownHasEnded()
    {
        countdownPanel.SetActive(false);
        scorePanel.SetActive(false);
        startGameButton.gameObject.SetActive(true);

        // remove all remaining prefabs (i.e. bones)
        var instantiatedPrefabs = GameObject.FindGameObjectsWithTag("clone");
        foreach (var clone in instantiatedPrefabs)
        {
            Destroy(clone);
        }
    }

    void SpawnBone()
    {
        float minX = -0.7f;
        float minZ = -0.7f;
        float maxX = 0.7f;
        float maxZ = 0.7f;

        float xpos = Random.Range(minX, maxX);
        float ypos = 0.05f;
        float zpos = Random.Range(minZ, maxZ);

        // make sure spawn location is not at an invalid place
        while ((xpos > 0.5 && ypos < -0.44) || (xpos < -0.5 && zpos > 0.5))
        {
            xpos = Random.Range(minX, maxX);
            zpos = Random.Range(minZ, maxZ);
        }        

        Vector3 posVector = new Vector3(xpos, ypos, zpos);

        GameObject instance = Instantiate(bonePrefab, posVector, Quaternion.identity);
        //instance.transform.parent = transform;
        instance.transform.SetParent(transform, false);

    }


    public void CollisionDetected(Collider col)
    {
        if (col.gameObject)
        {
            CollectedBone(col.gameObject);
        }
    }

    void CollectedBone(GameObject collidedBone)
    {
        if (collidedBone)
        {
            score += 1;

            //Debug.Log("+++++++++++ DESTROY ++++++++++++++");
            Destroy(collidedBone);

            // spawn new bone if game is not over yet
            if (countdownTimerIsActive == true)
            {
                SpawnBone();
            }
        }
    }


    // ###########################################################################################################
    // ################################### IVirtualButtonEventHandler ############################################
    // ###########################################################################################################

    public void OnButtonPressed(VirtualButtonBehaviour vb) {

    }

    public void OnButtonReleased(VirtualButtonBehaviour vb) {
        
    }



    // ###########################################################################################################
    // ##################################### ITrackableEventHandler ##############################################
    // ###########################################################################################################

    // used to activate/deactivate gameobjects and the application of gravity in order to avoid undesired movement
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();
        }
    }

    private void OnTrackingFound()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();


        for (int i = 0; i < transform.childCount; i++)
        {
            Debug.Log("Activating the children");
            this.transform.GetChild(i).gameObject.SetActive(true);
        }

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }

        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = true;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " found");
    }

    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        Collider[] colliderComponents = GetComponentsInChildren<Collider>();


        for (int i = 0; i < transform.childCount; i++)
        {
            Debug.Log("Deactivating children");
            this.transform.GetChild(i).gameObject.SetActive(false);
        }

        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }


        var rigidBody = GetComponentsInChildren<Rigidbody>(true);

        foreach (var component in rigidBody)
        {
            component.useGravity = false;
        }

        Debug.Log("Tracking of " + mTrackableBehavior.TrackableName + " lost");

    }
}
